import React from 'react'
import { Row } from 'antd';
import { get, post } from '../../utils';
import ProductCard from "../../component/ProductCard";
import ProductDetailModal from "../../component/ProductDetailModal";
import "./styles.scss"


class HomePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalVisible: false,
      tempProduct: null,
      productList: []
    };
  }


  getProductList() {
    let url = '/vending/machine/product'
    get(url)
      .then(responses => {
        if (responses.status >= 400) {
          throw new Error("Bad responses from server");
        }
        this.setState({ productList: responses })
      })
      .catch(err => {
        console.log(err.message)
      })
  }


  getGeolocation() {
    if ("geolocation" in navigator) {
      console.log("process.env.GOOGLE_API_KEY ", process.env.GOOGLE_API_KEY); 
      navigator.geolocation.getCurrentPosition(
        (position) => {
          console.log("Position : ", position);
          let url = '/vending/machine/vending/machine/location'
          let data = {location_latitude : position.coords.latitude, location_longitude: position.coords.longitude}
          post(url, data)
            .then(responses => {
              if (responses.status >= 400) {
                throw new Error("Bad responses from server");
              } 
            })
            .catch(err => {
              console.log(err.message)
            })

        },
        (error) => {
          console.error(error.message);
        }
      );
    } else {
      console.log("Not Available");
    }
  }


  componentDidMount() {
    this.getProductList()
    this.getGeolocation()
    this.getGeolocationInterval = setInterval(() => {
      this.getGeolocation()
    }, 1000 * 60 * 60);
  }

  componentWillUnmount(){
    clearInterval(this.getGeolocationInterval)
  }

  onSelectItem(item) {
    this.setState({ tempProduct: item, isModalVisible: true })
  }


  handleCancelModal() {
    this.setState({ isModalVisible: false })
  }

  handleOkModal() {
    this.getProductList()
    this.setState({ isModalVisible: false })
  }

  render() {
    const { isModalVisible, tempProduct, productList } = this.state
    return (
      <div className='home-page-wrapper'>
        <h1>{'Vending machine'}</h1>
        <Row
          type="flex" justify="space-around"
          className='product-list-component-wrapper'
        >
          {productList.map((item) => {
            return (
              <ProductCard
                key={item.id}
                {...item}
                onSelectItem={() => this.onSelectItem(item)}
              />
            )
          })}
          <ProductDetailModal
            product={tempProduct}
            isModalVisible={isModalVisible}
            handleOk={() => this.handleOkModal()}
            handleCancel={() => this.handleCancelModal()}
          />
        </Row>
      </div>
    )
  }
}

export default HomePage;