import React from 'react'
import { HashRouter as Router, Switch, Route } from "react-router-dom";
import HomePage from './container/HomePage' 
import './assets/styles/styles.scss'

class App extends React.Component {
 
    render() {
        return (
            <Router>
                <Switch>
                    <Route exact path="/" component={HomePage} />
                </Switch>
            </Router>
        )
    }
}

export default App;