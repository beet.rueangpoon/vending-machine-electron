import React from 'react'
import { Col } from 'antd';
import config from '../../../config.json';
import "./styles.scss"

const UPLOAD_URL = `${config.API_PROTOCOL}://${config.API_HOSTNAME}:${config.API_PORT}/uploads/`
class ProductCard extends React.Component {
  render() {
    const { name, image, quantity } = this.props
    return (
      <Col
        xs={24} sm={12} md={8} lg={8} xl={6}
        className='product-card-component-wrapper'
      >
        {quantity > 0 ? null : <div className='out-of-stock-wrapper'><p>{'Out of stock'}</p></div>}
        <div
          className='product-card-wrapper'
          style={{ opacity: quantity > 0 ? 1 : 0.2, cursor:  quantity > 0 ? "pointer" : 'not-allowed' }}
          onClick={quantity > 0 ? this.props.onSelectItem : null}
        >
          <img className='product-image' src={UPLOAD_URL + image} />
          <p className='product-name'>{name}</p> 
        </div>
      </Col>
    )
  }
}

export default ProductCard;