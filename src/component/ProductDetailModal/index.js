import React from 'react'
import { Button, Modal, Spin } from 'antd';
import { LeftCircleOutlined, RightCircleOutlined } from '@ant-design/icons';
import { post } from '../../utils';
import config from '../../../config.json';
import "./styles.scss"

0
const UPLOAD_URL = `${config.API_PROTOCOL}://${config.API_HOSTNAME}:${config.API_PORT}/uploads/`

class ProductDetailModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      quantity: 1,
    };
  }

  setLoadingStatus(status) {
    this.setState({ loading: status })
  }
  handleOk() {
    this.props.handleOk()
  }

  handleCancel() {
    this.props.handleCancel()
  }

  onClickIncreaseQuantity() {
    const { quantity } = this.state
    const { product } = this.props
    if (quantity < product.quantity) {
      this.setState({ quantity: quantity + 1 })
    }
  }

  onClickDecreaseQuantity() {
    const { quantity } = this.state
    if (quantity > 1) {
      this.setState({ quantity: quantity - 1 })
    }
  }

  onClickConfirm() {
    this.setLoadingStatus(true)
    const { product } = this.props
    const { quantity } = this.state
    let url = '/vending/machine/orders'
    let data = { product_id: product.id, quantity: quantity }
    post(url, data)
      .then(responses => {
        if (responses.status >= 400) {
          throw new Error("Bad responses from server");
        }
        setTimeout(() => {
          this.setLoadingStatus(false)
          this.props.handleOk()
        }, 2000);

      })
      .catch(err => {
        console.log(err.message)
        this.setLoadingStatus(false)
      })
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.isModalVisible !== this.props.isModalVisible) {
      this.setState({ quantity: 1 })
    }
  }
  render() {
    const { quantity, loading } = this.state
    const { isModalVisible, product } = this.props
    return (
      <Modal
        title={null}
        footer={null}
        visible={isModalVisible}
        onOk={() => this.handleOk()}
        onCancel={() => this.handleCancel()}
        width={1000}
      >
        <Spin spinning={loading} tip="Loading...">
          <div className='product-detail-modal-wrapper'>
            <div className='product-detail-wrapper' >
              {product ? <img className='product-image' src={UPLOAD_URL + product.image} /> : null}
              <p className='product-name'>{product ? product.name : ''}</p>
              <p className='product-quantity'>{product ? `Quantity : ${product.quantity}` : ''}</p>

            </div>

            <div className='action-quantity-button-wrapper'>
              <div
                className='decrease-quantity-button-wrapper'
                onClick={() => this.onClickDecreaseQuantity()}
              >
                <LeftCircleOutlined />
              </div>
              <div className='quantity-text-wrapper'>
                <p >{quantity}</p>
              </div>
              <div
                className='increase-quantity-button-wrapper'
                onClick={() => this.onClickIncreaseQuantity()}
              >
                <RightCircleOutlined />
              </div>
            </div>
            <div className='confirm-button-wrapper'>
              <Button className='confirm-button' type={'primary'} onClick={() => this.onClickConfirm()}>{'Confirm'}</Button>
            </div>
          </div>
        </Spin>
      </Modal>
    )
  }
}

export default ProductDetailModal;